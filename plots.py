import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import rc 

import numpy as np
FONTSIZE = 14
def main():
    rc('text', usetex=True)
    density = 998
    viscosity = 1.0035 * 10**-6

    data_180 = pd.read_csv("data/chan180/profiles/chan180.means", sep="  ", skiprows=25, index_col=None, engine="python")
    y_180 = data_180.iloc[:, 0]
    y_plus_180 = data_180.iloc[:, 1]
    u_180 = data_180.iloc[:, 2]
    viscous_stress_180 = data_180.iloc[:, 3] * density * viscosity
    data_395 = pd.read_csv("data/chan395/profiles/chan395.means", sep="  ", skiprows=25, index_col=None, engine="python")
    y_395 = data_395.iloc[:, 0]
    y_plus_395 = data_395.iloc[:, 1]
    u_395 = data_395.iloc[:, 2]
    viscous_stress_395 = data_395.iloc[:, 3] * density * viscosity
    data_590 = pd.read_csv("data/chan590/profiles/chan590.means", sep="  ", skiprows=25, index_col=None, engine="python")
    y_590 = data_590.iloc[:, 0]
    y_plus_590 = data_590.iloc[:, 1]
    u_590 = data_590.iloc[:, 2]
    viscous_stress_590 = data_590.iloc[:, 3] * density * viscosity
    
    data_180_stress = pd.read_csv("data/chan180/profiles/chan180.reystress", sep="  ", skiprows=25, index_col=None, engine="python")
    r_uu_180 = data_180_stress.iloc[:, 2]
    r_vv_180 = data_180_stress.iloc[:, 3]
    r_ww_180 = data_180_stress.iloc[:, 4]
    r_uv_180 = data_180_stress.iloc[:, 5]

    data_395_stress = pd.read_csv("data/chan395/profiles/chan395.reystress", sep="  ", skiprows=25, index_col=None, engine="python")
    r_uu_395 = data_395_stress.iloc[:, 2]
    r_vv_395 = data_395_stress.iloc[:, 3]
    r_ww_395 = data_395_stress.iloc[:, 4]
    r_uv_395 = data_395_stress.iloc[:, 5]

    data_590_stress = pd.read_csv("data/chan590/profiles/chan590.reystress", sep="  ", skiprows=25, index_col=None, engine="python")
    r_uv_590 = data_590_stress.iloc[:, 5]

    data_395_balance = pd.read_csv("data/chan395/balances/chan395.kbal", sep="  ", skiprows=25, index_col=None, engine="python")
    dissipation_395 = data_395_balance.iloc[:, 2]
    production_395 = data_395_balance.iloc[:, 3]
    pdiff_395 = data_395_balance.iloc[:, 5]
    tdiff_395 = data_395_balance.iloc[:, 6]
    vdiff_395 = data_395_balance.iloc[:, 7]

    # mean_velocity_profiles(y_180, y_395, y_590, u_180, u_395, u_590)
    stresses(y_180, y_395, y_590, viscous_stress_180, viscous_stress_395, viscous_stress_590, r_uv_180, r_uv_395, r_uv_590)
    # fractional_contribution(y_plus_180, y_plus_395, y_plus_590, viscous_stress_180, viscous_stress_395, viscous_stress_590, r_uv_180, r_uv_395, r_uv_590)
    # near_wall_profiles(y_plus_180, u_180, y_plus_395, u_395, y_plus_590, u_590)
    # reynolds_stresses(y_plus_395, r_uu_395, r_vv_395, r_ww_395, r_uv_395)
    # balances(y_plus_395, dissipation_395, production_395, pdiff_395, tdiff_395, vdiff_395)

def balances(y_plus, dissipation, production, pdiff, tdiff, vdiff):
    plt.figure()
    plt.plot(y_plus, dissipation, '--')
    plt.plot(y_plus, production)
    plt.plot(y_plus, pdiff, ':')
    plt.plot(y_plus, tdiff, '.')
    plt.plot(y_plus, vdiff, '-.')
    plt.xlim(0, 50)
    plt.axhline(0, color='black')
    plt.xlabel(r"$y^+$", fontsize=FONTSIZE)
    plt.ylabel(r"$TKE~Balanace$", fontsize=FONTSIZE)
    plt.legend([r"Dissipation", r"Production", r"Pressure transport", r"Turbulent convection", r"Viscous diffusion"], fontsize=8)
    plt.savefig("figure-7-18.png", dpi=600, bbox_inches='tight', transparent=True)

def reynolds_stresses(y_plus, r_uu, r_vv, r_ww, r_uv):
    tke = (1/2) * (r_uu + r_vv + r_ww)
    plt.figure()
    plt.plot(y_plus, r_uu)
    plt.plot(y_plus, r_vv, '--')
    plt.plot(y_plus, r_ww, '-.')
    plt.plot(y_plus, r_uv, ':')
    plt.plot(y_plus, tke, '.')
    plt.xlabel(r"$y^+$", fontsize=FONTSIZE)
    plt.ylabel(r"$\frac{\langle u_i u_j \rangle}{u_{\tau}^2}$", fontsize=FONTSIZE)
    plt.xlim(0, 50)
    plt.ylim(-1, 8)
    plt.legend([r"$\langle u^2 \rangle$", r"$\langle v^2 \rangle$", r"$\langle w^2 \rangle$", r"$k$"], fontsize=FONTSIZE)
    plt.savefig("figure-7-17.png", dpi=600, bbox_inches='tight', transparent=True)

def near_wall_profiles(y_plus_180, u_plus_180, y_plus_395, u_plus_395, y_plus_590, u_plus_590):
    """
    Av någon anledning så blir det fel om man skalar 
    u^+ = <U> / u_T
    när man bara plottar med <U> stämmer det precis med plottarna i Pope
    """
    plt.figure() # Figure 7.5
    plt.plot(y_plus_180, u_plus_180, 'o')
    plt.plot(y_plus_395, u_plus_395, 'd')
    # plt.plot(y_plus_590, u_plus_590, 'x')
    plt.plot(y_plus_180, y_plus_180)
    plt.xlim(0, 20)
    plt.ylim(0, 15)
    plt.xlabel(r"$y^+$", fontsize=FONTSIZE)
    plt.ylabel(r"$u^+$", fontsize=FONTSIZE)
    plt.legend([r"$Re=5600$", r"$Re=13750$", r"$y^+=u^+$"], fontsize=FONTSIZE)
    mng = plt.get_current_fig_manager()
    mng.full_screen_toggle()
    plt.savefig("figure-7-5.png", dpi=600, bbox_inches='tight', transparent=True)

    plt.figure() # Figure 7.7
    plt.plot(y_plus_180, u_plus_180, 'o')
    plt.plot(y_plus_395, u_plus_395, 'd')
    plt.plot(y_plus_395[15:], (1/0.41)*np.log(y_plus_395[15:])+5.2)
    plt.xlim(1, 1000)
    plt.xlabel(r"$y^+$", fontsize=FONTSIZE)
    plt.ylabel(r"$u^+$", fontsize=FONTSIZE)
    plt.legend([r"$Re=5600$", r"$Re=13750$", r"$u^+=\frac{1}{\kappa}ln(y^+) + B$"], fontsize=FONTSIZE)
    plt.xscale("log")
    plt.savefig("figure-7-7.png", dpi=600, bbox_inches='tight', transparent=True)

def fractional_contribution(y_plus_180, y_plus_395, y_plus_590, viscous_stress_180, viscous_stress_395, viscous_stress_590, r_uv_180, r_uv_395, r_uv_590):
    tau_180 = viscous_stress_180 / viscous_stress_180[0] - r_uv_180
    tau_395 = viscous_stress_395 / viscous_stress_395[0] - r_uv_395
    tau_590 = viscous_stress_590 / viscous_stress_590[0] - r_uv_590

    plt.figure()
    plt.plot(y_plus_180, viscous_stress_180 / viscous_stress_180[0] * tau_180, '--', color="blue")
    plt.plot(y_plus_395, viscous_stress_395 / viscous_stress_395[0] * tau_395, color="orange")
    # plt.plot(y_plus_590, viscous_stress_590 / viscous_stress_590[0] * tau_590, color="green")
    plt.plot(y_plus_180, -r_uv_180 / tau_180, '--', color="blue")
    plt.plot(y_plus_395, -r_uv_395 / tau_395, color="orange")
    # plt.plot(y_plus_590, -r_uv_590 / tau_590, color="green")
    plt.legend([r"$Re=5600$", r"$Re=13750$"], fontsize=FONTSIZE)
    plt.xlabel(r"$y^+$")
    plt.xlim(0, 60)
    # plt.savefig("figure-7-4.png", dpi=600, bbox_inches='tight', transparent=True)


def stresses(y_180, y_395, y_590, viscous_stress_180, viscous_stress_395, viscous_stress_590, r_uv_180, r_uv_395, r_uv_590):

    cfx_data_viscous_rsm = pd.read_csv("gustav-data/RSM5600_fig7-3_viscous_stress.csv", sep=",", skiprows=5, index_col=None, engine="python")
    cfx_data_viscous_sst = pd.read_csv("gustav-data/SST5600_fig7-3_viscous_stress.csv", sep=",", skiprows=5, index_col=None, engine="python")
    cfx_data_Reynolds_rsm = pd.read_csv("gustav-data/RSM5600_fig7-3_Reynolds_stress.csv", sep=",", skiprows=5, index_col=None, engine="python")
    cfx_data_Reynolds_sst = pd.read_csv("gustav-data/SST5600_fig7-3_Reynolds_stress.csv", sep=",", skiprows=5, index_col=None, engine="python")
    of_data_sst = pd.read_csv("OF_data/Re5600sst.xy", sep=",", skiprows=1, index_col=None, engine="python")
    of_data_rsm = pd.read_csv("OF_data/Re5600ssg.xy", sep=",", skiprows=1, index_col=None, engine="python")

    fig, (ax1, ax2) = plt.subplots(1,2)
    ax1.plot(y_180, viscous_stress_180 / viscous_stress_180[0], 'k', linewidth=3)
    ax1.plot(cfx_data_viscous_rsm.iloc[:,0] / 0.5, cfx_data_viscous_rsm.iloc[:,1] / cfx_data_viscous_rsm.iloc[0,1], "rx")
    ax1.plot(cfx_data_viscous_sst.iloc[:,0] / 0.5, cfx_data_viscous_sst.iloc[:,1] / cfx_data_viscous_sst.iloc[0,1], 'ro', markerfacecolor='none')
    ax1.plot(of_data_rsm.iloc[:100,0], of_data_rsm.iloc[:100,3] / of_data_rsm.iloc[0,3], "bx")
    ax1.plot(of_data_sst.iloc[:100,0], of_data_sst.iloc[:100,3] / of_data_sst.iloc[0,3], 'bo', markerfacecolor='none')
    # ax1.plot(y_395, viscous_stress_395 / viscous_stress_395[0], 'd')
    # ax1.plot(y_590, viscous_stress_590 / viscous_stress_590[0], 'x')
    ax1.plot(y_395, viscous_stress_395 / viscous_stress_395[0] - r_uv_395, 'g', linewidth=3)
    ax1.set_xlabel(r"$y/\delta$", fontsize=FONTSIZE)
    ax1.set_title(r"Viscous stress", fontsize=FONTSIZE)

    ax2.plot(y_180, -r_uv_180, 'k', linewidth=3)
    ax2.plot(cfx_data_Reynolds_rsm.iloc[:,0] / 0.5, cfx_data_Reynolds_rsm.iloc[:,1], "rx")
    ax2.plot(cfx_data_Reynolds_sst.iloc[:,0] / 0.5, cfx_data_Reynolds_sst.iloc[:,1], 'ro', markerfacecolor='none')
    ax2.plot(of_data_rsm.iloc[:100,0], -of_data_rsm.iloc[:100,6]/ (0.000357*of_data_rsm.iloc[0,3]), "bx")
    ax2.plot(of_data_sst.iloc[:100,0], -of_data_sst.iloc[:100,6]/ (0.000357*of_data_sst.iloc[0,3]), 'bo', markerfacecolor='none')
    # ax2.plot(y_395, -r_uv_395, 'd')
    # ax2.plot(y_590, -r_uv_590, 'x')
    ax2.plot(y_395, viscous_stress_395 / viscous_stress_395[0] - r_uv_395, 'g', linewidth=3) 

    ax2.set_title(r"Reynolds stress", fontsize=FONTSIZE)
    ax2.set_xlabel(r"$y/\delta$", fontsize=FONTSIZE)
    # ax2.legend([r"$Re=5600$", r"$Re=13750$", r"$\tau(y)/\tau_w$"], fontsize=FONTSIZE)
    # plt.savefig("figure-7-3.png", dpi=600, bbox_inches='tight', transparent=True)
    
    plt.figure()
    plt.plot(of_data_rsm.iloc[:100,0], -of_data_rsm.iloc[:100,6]/ (0.000357*of_data_rsm.iloc[0,3]), "bx")
    plt.plot(of_data_sst.iloc[:100,0], -of_data_sst.iloc[:100,6]/ (0.000357*of_data_sst.iloc[0,3]), 'bo', markerfacecolor='none')


def mean_velocity_profiles(y_180, y_395, y_590, u_180, u_395, u_590):
    u_bulk_180 = np.trapz(u_180, y_180) / y_180.iloc[-1]
    u_bulk_395 = np.trapz(u_395, y_395) / y_395.iloc[-1]
    plt.figure()
    plt.plot(y_180, u_180/u_bulk_180, 'o')
    plt.plot(y_395, u_395/u_bulk_395, 'd')
    # plt.plot(y_590, u_590/np.mean(u_590), 'x')
    plt.legend([r"$Re=5600$", r"$Re=13750$"], fontsize=FONTSIZE)
    plt.xlabel(r"$y/\delta$", fontsize=FONTSIZE)
    plt.ylabel(r"$\frac{\langle U \rangle}{\overline{U}}$", fontsize=FONTSIZE)
    # plt.savefig("figure-7-2.png", dpi=600, bbox_inches='tight', transparent=True)

    cfx_data_5600_rsm = pd.read_csv("gustav-data/RSM13700_fig7-2.csv", sep=",", skiprows=5, index_col=None, engine="python")
    cfx_data_5600_sst = pd.read_csv("gustav-data/SST13700_fig7-2.csv", sep=",", skiprows=5, index_col=None, engine="python")
    of_data_5600_sst = pd.read_csv("OF_data/Re13750sst.xy", sep=",", skiprows=1, index_col=None, engine="python")
    of_data_5600_rsm = pd.read_csv("OF_data/Re13750ssg.xy", sep=",", skiprows=1, index_col=None, engine="python")
    
    y_5600_rsm_of = of_data_5600_rsm.iloc[:100, 0]
    u_5600_rsm_of = of_data_5600_rsm.iloc[:100, 1]
    u_5600_bulk_rsm_of = np.trapz(u_5600_rsm_of, y_5600_rsm_of) / y_5600_rsm_of.iloc[-1]

    y_5600_sst_of = of_data_5600_sst.iloc[:100, 0]
    u_5600_sst_of = of_data_5600_sst.iloc[:100, 1]
    u_5600_bulk_sst_of = np.trapz(u_5600_sst_of, y_5600_sst_of) / y_5600_sst_of.iloc[-1]

    y_5600_rsm = cfx_data_5600_rsm.iloc[:, 0]
    u_5600_rsm = cfx_data_5600_rsm.iloc[:, 1]
    u_5600_bulk_rsm = np.trapz(u_5600_rsm, y_5600_rsm) / y_5600_rsm.iloc[-1]

    y_5600_sst = cfx_data_5600_sst.iloc[:, 0]
    u_5600_sst = cfx_data_5600_sst.iloc[:, 1]
    u_5600_bulk_sst = np.trapz(u_5600_sst, y_5600_sst) / y_5600_sst.iloc[-1]

    plt.figure()
    plt.plot(y_395, u_395/u_bulk_395, "k", linewidth="3")
    plt.plot(y_5600_rsm[::10]/y_5600_rsm.iloc[-1], u_5600_rsm[::10] / u_5600_bulk_rsm, 'rx')
    plt.plot(y_5600_sst[::10]/y_5600_sst.iloc[-1], u_5600_sst[::10] / u_5600_bulk_sst, 'ro', markerfacecolor='none')
    plt.plot(y_5600_rsm_of[::3]/y_5600_rsm_of.iloc[-1], u_5600_rsm_of[::3] / u_5600_bulk_rsm_of, 'bx')
    plt.plot(y_5600_sst_of[::3]/y_5600_sst_of.iloc[-1], u_5600_sst_of[::3] / u_5600_bulk_sst_of, 'bo', markerfacecolor='none')
    plt.ylim(0, 1.2)
    

    # plt.plot(y_395, u_395/u_bulk_395, 'd')
    plt.xlabel(r"$y/\delta$", fontsize=FONTSIZE)
    plt.ylabel(r"$\frac{\langle U \rangle}{\overline{U}}$", fontsize=FONTSIZE)
    plt.legend([r"DNS data", r"CFX RSM", r"CFX SST", r"OF RSM", r"OF SST"], fontsize=FONTSIZE)
    plt.savefig("figure-7-2_13750.png", dpi=600, bbox_inches='tight', transparent=True)

if __name__ == "__main__":
    main()
    plt.show()